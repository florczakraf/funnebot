import audio.PortAudio
import audio.loadAllSounds
import config.Config
import filesystem.createDirectory
import filesystem.fileExists
import filesystem.readFile
import filesystem.writeFile
import irc.Irc
import irc.loadCommands
import kotlinx.cinterop.toKString
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import tts.GoogleTTSConverter
import kotlin.system.exitProcess

private const val CONFIG_FILE_NAME = "config.json"
private const val STATUS_DIRECTORY_NAME = "status"
private const val SOUND_LIST_FILE_NAME = "soundlist.txt"
private const val COMMAND_LIST_FILE_NAME = "commandlist.txt"

fun main() = runBlocking {
    if (!fileExists(CONFIG_FILE_NAME)) {
        println("No config file found! Refer to setup instructions.")
        exitProcess(10)
    }
    val config = Json.decodeFromString<Config>(readFile(CONFIG_FILE_NAME).toKString())

    val sounds = config.soundsPath?.let { loadAllSounds(it) } ?: emptyMap()
    val commands = config.commandsPath?.let { loadCommands(it) } ?: emptyMap()
    createDirectory(STATUS_DIRECTORY_NAME)
    writeFile("$STATUS_DIRECTORY_NAME/$SOUND_LIST_FILE_NAME", sounds.keys.joinToString("\n") { "!$it" })
    writeFile("$STATUS_DIRECTORY_NAME/$COMMAND_LIST_FILE_NAME", commands.keys.joinToString("\n") { "!$it" })
    println("Loaded sounds: ${sounds.keys}")
    println("Loaded commands: ${commands.keys}")

    val audioPlayer = PortAudio()
    val irc = Irc(audioPlayer, commands, sounds, setOf(GoogleTTSConverter))
    irc.runIrcLoop(config.nick, config.oauth)
}
