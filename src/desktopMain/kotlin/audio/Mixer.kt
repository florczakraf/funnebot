package audio

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlin.math.min

object Mixer {

    @OptIn(ExperimentalCoroutinesApi::class)
    private val context = newSingleThreadContext("mixer")

    private val inputs: MutableList<Input> = mutableListOf()

    fun play(sound: Sound, queued: Boolean = false) = runBlocking(context) {
        inputs.add(Input(sound.samples, queued))
    }

    fun nextAudioSegment(frameCount: Int): FloatArray = runBlocking(context) {
        if (inputs.isEmpty()) {
            FloatArray(frameCount * 2)
        } else {
            val inputsToPlay = mutableListOf<Input>()
            inputs.firstOrNull { it.queued }?.let { inputsToPlay.add(it) }
            inputsToPlay.addAll(inputs.filter { !it.queued })

            val audioSegment = inputsToPlay.map { it.nextAudioSegment(frameCount) }
                .reduce { first, second ->
                    first.mapIndexed { index, i ->
                        i + second[index]
                    }.toFloatArray()
                }
                .map { it.coerceIn(-1.0f, 1.0f) }
                .toFloatArray()

            // TODO: this most likely lags the audio stream, implement some sweeper to remove empty inputs?
            inputs.removeAll { it.isFinished() }
            audioSegment
        }
    }
}

class Input(
    private val soundData: FloatArray,
    val queued: Boolean = false
) {
    private var bufferPosition: Int = 0

    fun nextAudioSegment(frameCount: Int): FloatArray {
        val mustRead = frameCount * 2 // 2 channels
        val result = FloatArray(mustRead)
        val endIndex = min(bufferPosition + mustRead, soundData.size)
        soundData.copyInto(result, 0, bufferPosition, endIndex)
        bufferPosition += mustRead
        return result
    }

    fun isFinished() = bufferPosition >= soundData.size
}
