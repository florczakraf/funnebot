package audio.decoders

import audio.Sound
import kotlinx.cinterop.*
import minimp3.mp3dec_file_info_t
import minimp3.mp3dec_load_buf
import minimp3.mp3dec_t
import platform.posix.memcpy

actual object Mp3AudioDecoder : AudioDecoder {

    @OptIn(ExperimentalUnsignedTypes::class)
    actual override fun decode(bytes: ByteArray): Sound {
        val data = bytes.asUByteArray()
        return memScoped {
            val mp3Decoder = alloc<mp3dec_t>()
            val info = alloc<mp3dec_file_info_t>()

            val status = mp3dec_load_buf(
                mp3Decoder.ptr,
                data.refTo(0),
                data.size.convert(),
                info.ptr,
                null,
                null
            )
            if (status != 0) {
                throw AudioDecoderException("Error while decoding MP3: $status")
            }

            val samplesCount = info.samples.toInt()
            val sizeInBytes = samplesCount * sizeOf<FloatVar>()
            val mp3Samples = FloatArray(samplesCount)
            memcpy(mp3Samples.refTo(0), info.buffer, sizeInBytes.convert())

            Sound(mp3Samples, info.channels, info.hz)
        }
    }
}
