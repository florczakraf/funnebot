package irc

import kotlin.test.Test
import kotlin.test.assertEquals

class ChatCommandTest {

    @Test
    fun `parses source out of raw irc source string`() {
        // given
        val rawIrcSourceString = "chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv"

        // when
        val source = Source.parseFrom(rawIrcSourceString)

        // then
        val expectedSource = Source("chazoshtare", "chazoshtare@chazoshtare.tmi.twitch.tv")
        assertEquals(expectedSource, source)
    }

    @Test
    fun `parses irc data out of raw irc data string`() {
        // given
        val rawIrcDataString = "PRIVMSG #chazoshtare"

        // when
        val ircData = IrcData.parseFrom(rawIrcDataString)

        // then
        val expectedIrcData = IrcData("PRIVMSG", "chazoshtare")
        assertEquals(expectedIrcData, ircData)
    }

    @Test
    fun `parses broadcaster's tags out of raw irc tags string`() {
        // given
        val rawIrcTagsString = "badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;client-nonce=29185e4ab41f8679eeffa1a97d560ff9;" +
                "color=#FF69B4;display-name=Chazoshtare;emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;room-id=87486142;" +
                "subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type="

        // when
        val tags = Tags.parseFrom(rawIrcTagsString)

        // then
        val expectedTags = Tags(
            Badges(
                broadcaster = true,
                vip = false
            ),
            moderator = false,
            subscriber = true
        )
        assertEquals(expectedTags, tags)
    }

    @Test
    fun `parses all false tags out of raw irc tags string`() {
        // given
        val rawIrcTagsString = "badge-info=;badges=;color=#1E90FF;display-name=Regular;emotes=;first-msg=0;flags=;id=21d88a90-cf11-4ac7-88fb-30b3cd3060e9;" +
                "mod=0;returning-chatter=0;room-id=87486142;subscriber=0;tmi-sent-ts=1674909764397;turbo=0;user-id=31100556;user-type="

        // when
        val tags = Tags.parseFrom(rawIrcTagsString)

        // then
        val expectedTags = Tags(
            Badges(
                broadcaster = false,
                vip = false
            ),
            moderator = false,
            subscriber = false
        )
        assertEquals(expectedTags, tags)
    }

    @Test
    fun `parses vip tags out of raw irc tags string`() {
        // given
        val rawIrcTagsString = "badge-info=;badges=vip/1;color=#1E90FF;display-name=SomeVip;emotes=;first-msg=0;flags=;" +
                "id=21d88a90-cf11-4ac7-88fb-30b3cd3060e9;mod=0;returning-chatter=0;room-id=87486142;subscriber=0;tmi-sent-ts=1674909764397;turbo=0;" +
                "user-id=31100556;user-type="

        // when
        val tags = Tags.parseFrom(rawIrcTagsString)

        // then
        val expectedTags = Tags(
            Badges(
                broadcaster = false,
                vip = true
            ),
            moderator = false,
            subscriber = false
        )
        assertEquals(expectedTags, tags)
    }

    @Test
    fun `parses mod tags out of raw irc tags string`() {
        // given
        val rawIrcTagsString = "badge-info=;badges=;color=#1E90FF;display-name=SomeMod;emotes=;first-msg=0;flags=;id=21d88a90-cf11-4ac7-88fb-30b3cd3060e9;" +
                "mod=1;returning-chatter=0;room-id=87486142;subscriber=0;tmi-sent-ts=1674909764397;turbo=0;user-id=31100556;user-type="

        // when
        val tags = Tags.parseFrom(rawIrcTagsString)

        // then
        val expectedTags = Tags(
            Badges(
                broadcaster = false,
                vip = false
            ),
            moderator = true,
            subscriber = false
        )
        assertEquals(expectedTags, tags)
    }
}
