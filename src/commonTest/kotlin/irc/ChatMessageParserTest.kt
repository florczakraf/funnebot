package irc

import tts.LanguageCode
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ChatMessageParserTest {

    private val availableAutoReplyCommands = setOf("autoreply")
    private val availableSounds = setOf("sound")
    private val availableLanguageCodes = setOf(LanguageCode("us"))

    private val chatMessageParser = ChatMessageParser(availableAutoReplyCommands, availableSounds, availableLanguageCodes)

    @Test
    fun `parses auto reply command out of full twitch string`() {
        // given
        val rawIrcString = "@mod=1 :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :!autoreply-oops test"

        // when
        val command = chatMessageParser.parseFromRawIrcString(rawIrcString)

        // then
        val expectedSource = Source("chazoshtare", "chazoshtare@chazoshtare.tmi.twitch.tv")
        val expectedIrcData = IrcData("PRIVMSG", "chazoshtare")
        val expectedTags = Tags(moderator = true)
        val expectedCommand = AutoReplyCommand(expectedSource, expectedIrcData, expectedTags, "!autoreply-oops test")
        assertEquals(listOf(expectedCommand), command)
    }

    @Test
    fun `parses sound command out of full twitch string`() {
        // given
        val rawIrcString = "@mod=1 :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :!sound-r-c50-r-lq-s some sound"

        // when
        val command = chatMessageParser.parseFromRawIrcString(rawIrcString)

        // then
        val expectedSource = Source("chazoshtare", "chazoshtare@chazoshtare.tmi.twitch.tv")
        val expectedIrcData = IrcData("PRIVMSG", "chazoshtare")
        val expectedTags = Tags(moderator = true)
        val expectedCommand = SoundCommand(expectedSource, expectedIrcData, expectedTags, "!sound-r-c50-r-lq-s some sound")
        assertEquals(listOf(expectedCommand), command)
    }

    @Test
    fun `parses tts command out of full twitch string`() {
        // given
        val rawIrcString = "@mod=1 :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :!us-er-r some test TTS message"

        // when
        val command = chatMessageParser.parseFromRawIrcString(rawIrcString)

        // then
        val expectedSource = Source("chazoshtare", "chazoshtare@chazoshtare.tmi.twitch.tv")
        val expectedIrcData = IrcData("PRIVMSG", "chazoshtare")
        val expectedTags = Tags(moderator = true)
        val expectedCommand = TtsCommand(expectedSource, expectedIrcData, expectedTags, "!us-er-r some test TTS message")
        assertEquals(listOf(expectedCommand), command)
    }

    @Test
    fun `does not parse any command out of twitch string not requesting for anything`() {
        // given
        val rawIrcString = "@mod=1 :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :just a normal message"

        // when
        val command = chatMessageParser.parseFromRawIrcString(rawIrcString)

        // then
        assertTrue(command.isEmpty())
    }
}
