package audio.decoders

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class WavAudioDecoderTest {

    @Test
    fun `decoding valid wav sound throws no exceptions`() {
        // given
        val soundBytes = TestSoundProvider.stereo44100Wav

        // when
        val decodedSound = WavAudioDecoder.decode(soundBytes)

        // then
        assertEquals(2, decodedSound.channels)
    }

    @Test
    fun `decoding invalid wav sound throws decoder exception`() {
        // given
        val soundBytes = ByteArray(10)

        // when
        // then
        assertFailsWith<AudioDecoderException> {
            WavAudioDecoder.decode(soundBytes)
        }
    }
}
