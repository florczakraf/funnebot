package audio.decoders

import filesystem.readFile

object TestSoundProvider {

    private const val TEST_SOUNDS_BASE_PATH = "src/commonTest/resources/sounds/"

    val stereo44100Wav by lazy { readFile(TEST_SOUNDS_BASE_PATH + "stereo44100.wav") }
    val stereo44100Mp3 by lazy { readFile(TEST_SOUNDS_BASE_PATH + "stereo44100.mp3") }
}
