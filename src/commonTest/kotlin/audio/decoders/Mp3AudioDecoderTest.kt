package audio.decoders

import kotlin.test.Test
import kotlin.test.assertEquals

class Mp3AudioDecoderTest {

    @Test
    fun `decoding valid mp3 file throws no exceptions`() {
        // given
        val soundBytes = TestSoundProvider.stereo44100Mp3

        // when
        val decodedSound = Mp3AudioDecoder.decode(soundBytes)

        // then
        assertEquals(2, decodedSound.channels)
    }
}
