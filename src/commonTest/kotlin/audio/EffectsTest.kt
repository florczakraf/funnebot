package audio

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertSame

private const val ANY_SAMPLE_RATE = 44100

class EffectsTest {

    @Test
    fun `none does not change the sound`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f), 1, ANY_SAMPLE_RATE)

        // when
        val sound = None.applyOn(initialSound)

        // then
        assertSame(initialSound, sound)
    }

    @Test
    fun `reverse reverses the sound`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f), 1, ANY_SAMPLE_RATE)

        // when
        val reversedSound = Reverse.applyOn(initialSound)

        // then
        val expectedSound = Sound(floatArrayOf(0.3f, 0.2f, 0.1f), 1, ANY_SAMPLE_RATE)
        assertEquals(expectedSound, reversedSound)
    }

    @Test
    fun `reverse reverses each channel of a sound separately`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f), 2, ANY_SAMPLE_RATE)

        // when
        val reversedSound = Reverse.applyOn(initialSound)

        // then
        val expectedSound = Sound(floatArrayOf(0.5f, 0.6f, 0.3f, 0.4f, 0.1f, 0.2f), 2, ANY_SAMPLE_RATE)
        assertEquals(expectedSound, reversedSound)
    }

    @Test
    fun `cut with defaults returns entire sound if it's shorter than default samples limit`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f), 2, ANY_SAMPLE_RATE)

        // when
        val cutSound = Cut().applyOn(initialSound)

        // then
        assertEquals(initialSound, cutSound)
    }

    @Test
    fun `cut returns a sound with sample size divisible by channel count`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f), 2, ANY_SAMPLE_RATE)

        // when
        val cutSound = Cut(50).applyOn(initialSound)

        // then
        val expectedSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f), 2, ANY_SAMPLE_RATE)
        assertEquals(expectedSound, cutSound)
    }

    @Test
    fun `cut returns at least one full frame when very short cut was requested`() {
        // given
        val initialSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f), 2, ANY_SAMPLE_RATE)

        // when
        val cutSound = Cut(1).applyOn(initialSound)

        // then
        val expectedSound = Sound(floatArrayOf(0.1f, 0.2f), 2, ANY_SAMPLE_RATE)
        assertEquals(expectedSound, cutSound)
    }
}
