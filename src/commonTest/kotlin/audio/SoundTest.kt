package audio

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class SoundTest {

    @Test
    fun `adds two sounds together`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f), 1, 44100)
        val secondSound = Sound(floatArrayOf(0.4f, 0.5f, 0.6f), 1, 44100)

        // when
        val sumSound = firstSound + secondSound

        // then
        val expectedSamples = floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f)
        val expectedSound = Sound(expectedSamples, 0.6f, 1, 44100)
        assertEquals(expectedSound, sumSound)
    }

    @Test
    fun `does not add sounds with different number of channels`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f), 1, 44100)
        val secondSound = Sound(floatArrayOf(0.4f, 0.5f), 2, 44100)

        // then
        assertFailsWith<IllegalArgumentException> {
            firstSound + secondSound
        }
    }

    @Test
    fun `does not add sounds with different sample rates`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f), 1, 44100)
        val secondSound = Sound(floatArrayOf(0.4f, 0.5f), 1, 22050)

        // then
        assertFailsWith<IllegalArgumentException> {
            firstSound + secondSound
        }
    }

    @Test
    fun `appends second sound after first ends`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f), 2, 44100)
        val secondSound = Sound(floatArrayOf(0.3f, 0.4f, 0.5f, 0.6f), 2, 44100)

        // when
        val resultSound = firstSound.append(secondSound)

        // then
        val expectedSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.3f, 0.4f, 0.5f, 0.6f), 2, 44100)
        assertEquals(expectedSound, resultSound)
    }

    @Test
    fun `appends second sound after some frames of first and mixes them both`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.3f, 0.4f), 2, 44100)
        val secondSound = Sound(floatArrayOf(0.35f, 0.45f, 0.55f, 0.65f), 2, 44100)

        // when
        val resultSound = firstSound.append(secondSound, 2)

        // then
        val expectedSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.65f, 0.85f, 0.55f, 0.65f), 2, 44100)
        assertEquals(expectedSound, resultSound)
    }

    @Test
    fun `mixes two sounds if appended was shorter than first`() {
        // given
        val firstSound = Sound(floatArrayOf(0.11f, 0.12f, 0.13f, 0.14f, 0.15f, 0.16f, 0.17f, 0.18f), 2, 44100)
        val secondSound = Sound(floatArrayOf(0.35f, 0.45f, 0.55f, 0.65f), 2, 44100)

        // when
        val resultSound = firstSound.append(secondSound, 1)

        // then
        val expectedSound = Sound(
            floatArrayOf(
                0.11f, 0.12f,
                0.13f + 0.35f, 0.14f + 0.45f, 0.15f + 0.55f, 0.16f + 0.65f, // mixed
                0.17f, 0.18f
            ), 2, 44100
        )
        assertEquals(expectedSound, resultSound)
    }

    @Test
    fun `appending keeps the sound between -1 and 1`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, -0.6f, 0.6f), 2, 44100)
        val secondSound = Sound(floatArrayOf(-0.7f, 0.7f, 0.5f, 0.6f), 2, 44100)

        // when
        val resultSound = firstSound.append(secondSound, 1)

        // then
        val expectedSound = Sound(floatArrayOf(0.1f, 0.2f, -1f, 1f, 0.5f, 0.6f), 2, 44100)
        assertEquals(expectedSound, resultSound)
    }

    @Test
    fun `does not append sounds with different number of channels`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.3f, 0.4f), 2, 44100)
        val secondSound = Sound(floatArrayOf(0.35f, 0.45f, 0.55f, 0.65f), 1, 44100)

        // then
        assertFailsWith<IllegalArgumentException> {
            firstSound.append(secondSound, 2)
        }
    }

    @Test
    fun `does not append sounds with different sample rates`() {
        // given
        val firstSound = Sound(floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f, 0.3f, 0.4f), 2, 44100)
        val secondSound = Sound(floatArrayOf(0.35f, 0.45f, 0.55f, 0.65f), 2, 22500)

        // then
        assertFailsWith<IllegalArgumentException> {
            firstSound.append(secondSound, 2)
        }
    }

    @Test
    fun `changes sound volume by given factor`() {
        // given
        val sound = Sound(floatArrayOf(0.1f, 0.2f, 0.25f), 1, 44100)
        val factor = 3.0f

        // when
        val amplifiedSound = sound.changeVolume(factor)

        // then
        val expectedSound = Sound(floatArrayOf(0.3f, 0.6f, 0.75f), 1, 44100)
        assertEquals(expectedSound, amplifiedSound)
    }

    @Test
    fun `changing volume keeps the sound between -1 and 1`() {
        // given
        val sound = Sound(floatArrayOf(-0.7f, -0.2f, 0.0f, 0.3f, 0.6f), 1, 44100)

        // when
        val amplifiedSound = sound.changeVolume(2.0f)

        // then
        val expectedSound = Sound(floatArrayOf(-1f, -0.4f, 0.0f, 0.6f, 1f), 1, 44100)
        assertEquals(expectedSound, amplifiedSound)
    }

    @Test
    fun `converts mono sound to stereo`() {
        // given
        val monoSamples = floatArrayOf(0.1f, 0.2f, 0.3f)
        val monoSound = Sound(monoSamples, 1, 44100)

        // when
        val stereoSound = monoSound.convertToStereo()

        // then
        val expectedSamples = floatArrayOf(0.1f, 0.1f, 0.2f, 0.2f, 0.3f, 0.3f)
        val expectedSound = Sound(expectedSamples, 0.3f, 2, 44100)
        assertEquals(expectedSound, stereoSound)
    }

    @Test
    fun `does not change the sound when converting stereo to stereo`() {
        // given
        val stereoSound = Sound(floatArrayOf(0.1f, 0.2f), 2, 44100)

        // when
        val convertedSound = stereoSound.convertToStereo()

        // then
        assertEquals(stereoSound, convertedSound)
    }

    @Test
    fun `resampling 22050hz sound to 44100hz generates twice more samples`() {
        // given
        val lowRateSamples = floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f)
        val lowRateSound = Sound(lowRateSamples, 1, 22050)

        // when
        val resampledSound = lowRateSound.resample(44100)

        // then
        assertEquals(lowRateSamples.size * 2, resampledSound.samples.size)
        assertEquals(44100, resampledSound.sampleRate)
    }

    @Test
    fun `resampling 44100hz sound to 22050hz generates half as many samples`() {
        // given
        val highRateSamples = floatArrayOf(0.1f, 0.2f, 0.3f, 0.4f)
        val highRateSound = Sound(highRateSamples, 1, 44100)

        // when
        val resampledSound = highRateSound.resample(22050)

        // then
        assertEquals(highRateSamples.size, resampledSound.samples.size * 2)
        assertEquals(22050, resampledSound.sampleRate)
    }
}
