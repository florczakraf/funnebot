package audio

import kotlinx.cinterop.*
import platform.posix.memcpy
import portaudio.*

actual class PortAudio : AudioPlayer {
    private val paStream: COpaquePointerVar = nativeHeap.alloc()

    init {
        startAudioStream()
    }

    actual override fun play(sound: Sound, queued: Boolean) {
        Mixer.play(sound, queued)
    }

    private fun startAudioStream() {
        if (Pa_Initialize() != paNoError) {
            println("Problem initializing PortAudio!")
            return
        }

        val error = Pa_OpenDefaultStream(
            paStream.ptr,
            0,
            2,
            paFloat32,
            DEFAULT_SAMPLE_RATE.toDouble(),
            512,
            staticCFunction(::portAudioCallback),
            null
        )
        if (error != paNoError) {
            println("Problem opening Default Stream: $error")
            println(Pa_GetErrorText(error)!!.toKString())
            return
        }

        if (Pa_StartStream(paStream.value) != paNoError) {
            println("Problem opening starting Stream: $error")
            println(Pa_GetErrorText(error)!!.toKString())
            return
        }
    }

    override fun close() {
        if (Pa_CloseStream(paStream.value) != paNoError) {
            println("Problem closing stream")
            return
        }

        if (Pa_Terminate() != paNoError) {
            println("Problem terminating PortAudio!")
            return
        }
        nativeHeap.free(paStream)
    }
}

@Suppress("UNUSED_PARAMETER")
private fun portAudioCallback(
    input: COpaquePointer?,
    output: COpaquePointer?,
    frameCount: ULong,
    timeInfo: CPointer<PaStreamCallbackTimeInfo>?,
    statusFlags: PaStreamCallbackFlags,
    userData: COpaquePointer?
): Int {
    val audioSegment = Mixer.nextAudioSegment(frameCount.convert())
    memcpy(output, audioSegment.refTo(0), frameCount * sizeOf<FloatVar>().convert() * 2u)

    return paContinue.toInt()
}
