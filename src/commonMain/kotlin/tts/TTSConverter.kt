package tts

import audio.Sound

interface TTSConverter {

    val supportedCodes: Set<LanguageCode>

    suspend fun convertToSound(text: String, languageCode: LanguageCode): Sound?
}
