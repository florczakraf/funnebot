package tts

import audio.DEFAULT_SAMPLE_RATE
import audio.Sound
import audio.decoders.Mp3AudioDecoder
import audio.resample
import httpClient
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.serialization.*
import io.ktor.util.*
import kotlinx.serialization.Serializable

// Currently disabled - TikTok now requires sessionId
// https://github.com/oscie57/tiktok-voice#session-id
object TikTokTTSConverter : TTSConverter {

    private const val TIKTOK_TTS_BASE_URL = "https://api16-normal-useast5.us.tiktokv.com/media/api/text/speech/invoke/"

    private const val volume: Float = 0.7f

    private val codesWithTikTokValues = mapOf(
        "ttfun" to "en_male_funny",
        "ttus" to "en_us_001",
        "ttuk" to "en_uk_001",
        "ttst" to "en_us_stormtrooper"
    )

    override val supportedCodes: Set<LanguageCode> = codesWithTikTokValues.keys.map { LanguageCode(it) }.toSet()

    override suspend fun convertToSound(text: String, languageCode: LanguageCode): Sound? {
        if (text.isBlank()) return null

        val response = httpClient.post(TIKTOK_TTS_BASE_URL) {
            parameter("text_speaker", codesWithTikTokValues[languageCode.value] ?: "en_us_001")
            parameter("req_text", text)
        }

        val sound = try {
            response.body<TikTokTTSResponse>()
        } catch (ex: JsonConvertException) {
            println("Error while parsing TikTok TTS response: ${ex.message}")
            println("Response from API: ${response.bodyAsText()}")
            null
        }

        return sound?.let {
            Mp3AudioDecoder.decode(it.data.v_str.decodeBase64Bytes())
                .resample(DEFAULT_SAMPLE_RATE)
                .changeVolume(volume)
                .convertToStereo()
        }
    }
}

@Serializable
private data class TikTokTTSResponse(
    val data: TikTokData
)

@Serializable
private data class TikTokData(
    val s_key: String,
    val v_str: String,
    val duration: String,
    val speaker: String
)
