package tts

import audio.DEFAULT_SAMPLE_RATE
import audio.Sound
import audio.decoders.Mp3AudioDecoder
import audio.resample
import httpClient
import io.ktor.client.request.*
import io.ktor.client.statement.*

object GoogleTTSConverter : TTSConverter {

    private const val GOOGLE_TTS_BASE_URL = "https://translate.google.com/translate_tts"

    private val codesWithGoogleValues = mapOf(
        "us" to "en_US", "gb" to "en_GB", "au" to "en_AU", "it" to "it_IT",
        "pl" to "pl_PL", "uk" to "uk_UA", "vi" to "vi_VN", "jp" to "ja_JP",
        "dk" to "da_DK", "nl" to "nl_NL", "fi" to "fi_FI", "ca" to "fr_CA",
        "fr" to "fr_FR", "de" to "de_DE", "kr" to "ko_KR", "no" to "nb_NO",
        "pt" to "pt_PT", "ru" to "ru_RU", "sk" to "sk_SK", "sv" to "sv_SE",
        "tr" to "tr_TR", "es" to "es_ES", "ar" to "es_AR", "in" to "en_IN",
        "hi" to "hi_IN", "cn" to "cmn_TW", "is" to "is_IS", "af" to "af_ZA",
        "en" to "en_US"
    )

    override val supportedCodes: Set<LanguageCode> = codesWithGoogleValues.keys.map { LanguageCode(it) }.toSet()

    override suspend fun convertToSound(text: String, languageCode: LanguageCode): Sound? {
        if (text.isBlank()) return null

        val response = httpClient.get(GOOGLE_TTS_BASE_URL) {
            parameter("client", "tw-ob")
            parameter("q", text)
            parameter("tl", codesWithGoogleValues[languageCode.value] ?: "en_US")
        }

        return Mp3AudioDecoder.decode(response.readBytes())
            .resample(DEFAULT_SAMPLE_RATE)
            .convertToStereo()
    }
}
