package audio

import audio.decoders.AudioDecoderFactory
import filesystem.*
import okio.Path

// TODO: read files in parallel
fun loadAllSounds(rootPath: String): Map<String, Sound> =
    if (fileExists(rootPath)) {
        listFilesInDirectory(rootPath)
            .filter { AudioDecoderFactory.getSupportedTypes().contains(it.extension().lowercase()) }
            .associate { it.fileNameWithoutExtension() to loadSound(it) }
    } else {
        println("Unable to load sounds - directory \"$rootPath\" does not exist!")
        emptyMap()
    }

private fun loadSound(path: Path): Sound {
    return AudioDecoderFactory.getDecoderForFile(path)
        .decode(readFile(path))
        .resample(DEFAULT_SAMPLE_RATE)
        .convertToStereo()
}
