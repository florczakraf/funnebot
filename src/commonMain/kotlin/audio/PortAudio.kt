package audio

expect class PortAudio() : AudioPlayer {

    override fun play(sound: Sound, queued: Boolean)
}
