package audio.decoders

import audio.Sound

interface AudioDecoder {

    fun decode(bytes: ByteArray): Sound
}
