package audio.decoders

import filesystem.extension
import okio.Path

object AudioDecoderFactory {

    fun getSupportedTypes() = setOf("wav", "mp3")

    fun getDecoderForFile(path: Path) =
        when (path.extension().lowercase()) {
            "wav" -> WavAudioDecoder
            "mp3" -> Mp3AudioDecoder
            else -> throw UnsupportedAudioFormatException("Unsupported audio format: ${path.extension()}")
        }
}

class UnsupportedAudioFormatException(message: String) : Exception(message)
