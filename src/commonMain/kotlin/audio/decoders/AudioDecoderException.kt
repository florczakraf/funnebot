package audio.decoders

class AudioDecoderException(message: String) : Exception(message)
