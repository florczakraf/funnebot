package audio.decoders

import audio.Sound

expect object WavAudioDecoder : AudioDecoder {

    actual override fun decode(bytes: ByteArray): Sound
}
