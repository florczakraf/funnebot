package audio.decoders

import audio.Sound

expect object Mp3AudioDecoder : AudioDecoder {

    actual override fun decode(bytes: ByteArray): Sound
}
