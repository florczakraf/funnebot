package audio

const val DEFAULT_SAMPLE_RATE = 44100

interface AudioPlayer {

    fun play(sound: Sound, queued: Boolean = false)

    fun close() {}
}
