package audio

val availableEffects = setOf(
    Reverse.NAMES,
    Cut.NAMES,
    LowQuality.NAMES,
    Slow.NAMES,
    Fast.NAMES,
    Earrape.NAMES
)

fun effectFromName(effectNameWithLevel: String): Effect {
    val pattern = Regex("(([A-Za-z]+)([0-9]*))")
    val match = pattern.find(effectNameWithLevel) ?: return None
    val (_, effectName, level) = match.destructured
    return when (effectName) {
        in Reverse.NAMES -> Reverse
        in Cut.NAMES -> Cut(level.toIntOrNull())
        in Slow.NAMES -> Slow
        in Fast.NAMES -> Fast
        in LowQuality.NAMES -> LowQuality
        in Earrape.NAMES -> Earrape
        else -> None
    }
}

sealed interface Effect {
    fun applyOn(sound: Sound): Sound
}

object None : Effect {
    override fun applyOn(sound: Sound) = sound
}

object Reverse : Effect {
    val NAMES = setOf("r", "reverse")

    override fun applyOn(sound: Sound): Sound {
        val channels = sound.channels
        val samples = sound.samples
        val lastIndex = samples.lastIndex
        val reversedSamples = FloatArray(samples.size)
        for (i in 0..lastIndex step channels) {
            repeat(channels) {
                val reversedSampleIndex = lastIndex - i - channels + 1 + it
                reversedSamples[reversedSampleIndex] = samples[i + it]
            }
        }
        return sound.copy(samples = reversedSamples)
    }
}

class Cut(private val level: Int? = null) : Effect {

    override fun applyOn(sound: Sound): Sound {
        val samples = sound.samples
        val cutPoint = if (level != null) {
            val actualLevel = level.coerceIn(1..99)
            (samples.size * actualLevel / 100).coerceAtLeast(1)
        } else DEFAULT_SAMPLES_LIMIT

        return if (cutPoint < samples.size) {
            val channels = sound.channels
            val unpairedSamples = cutPoint % channels
            val end = if (unpairedSamples == 0) cutPoint else cutPoint + (channels - unpairedSamples)
            Sound(samples.sliceArray(0 until end), channels, sound.sampleRate)
        } else sound
    }

    companion object {
        private const val DEFAULT_SAMPLES_LIMIT = 20000

        val NAMES = setOf("c", "cut")
    }
}

object Slow : ChangeSpeed(0.7f) {
    val NAMES = setOf("s", "slow")
}

object Fast : ChangeSpeed(1.3f) {
    val NAMES = setOf("f", "fast")
}

abstract class ChangeSpeed(private val factor: Float) : Effect {
    override fun applyOn(sound: Sound): Sound {
        // since audio player operates at a stable sample rate, resampling will artificially extend or shorten the sound
        val initialSampleRate = sound.sampleRate
        val targetSampleRate = (initialSampleRate / factor).toInt()
        return sound.resample(targetSampleRate).copy(sampleRate = initialSampleRate)
    }
}

object LowQuality : Effect {
    private const val DEFAULT_TARGET_SAMPLE_RATE = 8000

    val NAMES = setOf("lq", "lowquality")

    override fun applyOn(sound: Sound) = sound.resample(DEFAULT_TARGET_SAMPLE_RATE).resample(DEFAULT_SAMPLE_RATE)
}

object Earrape : Effect {
    private const val DEFAULT_FACTOR = 40f
    private const val DEFAULT_VOLUME_ADJUSTMENT = 0.17f

    val NAMES = setOf("er", "earrape")

    override fun applyOn(sound: Sound): Sound {
        val previousHighestPeak = sound.highestPeak
        val amplified = sound.changeVolume(DEFAULT_FACTOR)
        val newHighestPeak = amplified.highestPeak

        val deamplifyFactor = previousHighestPeak / newHighestPeak
        return amplified.changeVolume(deamplifyFactor * DEFAULT_VOLUME_ADJUSTMENT)
    }
}
