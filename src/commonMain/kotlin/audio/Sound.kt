package audio

import kotlin.math.abs

data class Sound(
    val samples: FloatArray,
    val highestPeak: Float,
    val channels: Int,
    val sampleRate: Int
) {
    val frames = samples.size / channels

    constructor(samples: FloatArray, channels: Int, sampleRate: Int) : this(
        samples,
        samples.let {
            val max = samples.maxOrNull()
            val absMin = samples.minOrNull()?.let { abs(it) }
            if (max == null || absMin == null) {
                return@let 0f
            }

            maxOf(max, absMin)
        },
        channels,
        sampleRate
    )

    operator fun plus(other: Sound): Sound {
        if (this.channels != other.channels) {
            throw IllegalArgumentException("Cannot add sounds with different number of channels!")
        }
        if (this.sampleRate != other.sampleRate) {
            throw IllegalArgumentException("Cannot add sounds with different sample rates!")
        }

        return this.copy(
            samples = this.samples + other.samples,
            highestPeak = maxOf(this.highestPeak, other.highestPeak)
        )
    }

    fun append(other: Sound, afterFrames: Int = this.frames): Sound {
        if (this.channels != other.channels) {
            throw IllegalArgumentException("Cannot append sound with different number of channels!")
        }
        if (this.sampleRate != other.sampleRate) {
            throw IllegalArgumentException("Cannot append sound with different sample rates!")
        }
        if (afterFrames == this.frames) {
            return this + other
        }

        if (afterFrames * this.channels + other.samples.size <= this.samples.size) {
            return mixWithShorter(other, afterFrames)
        } else {
            val firstSamplesSize = this.samples.size
            val afterSamples = afterFrames * this.channels
            val mixedSamplesCount = firstSamplesSize - afterSamples
            val finalSamples = FloatArray(afterSamples + other.samples.size)

            this.samples.copyInto(finalSamples, 0, 0, afterSamples)
            repeat(mixedSamplesCount) {
                val index = afterSamples + it
                finalSamples[index] = (this.samples[index] + other.samples[it]).coerceIn(-1f, 1f)
            }
            other.samples.copyInto(finalSamples, firstSamplesSize, mixedSamplesCount)
            return Sound(finalSamples, this.channels, this.sampleRate)
        }
    }

    /**
     * Mixes [other] into this sound, assuming [other] is shorter than the rest of this sound after [afterFrames].
     */
    private fun mixWithShorter(other: Sound, afterFrames: Int): Sound {
        val afterSamples = afterFrames * this.channels
        val finalSamples = this.samples.copyOf()

        other.samples.forEachIndexed { i, otherSample ->
            val index = afterSamples + i
            finalSamples[index] = (finalSamples[index] + otherSample).coerceIn(-1f, 1f)
        }

        return Sound(finalSamples, this.channels, this.sampleRate)
    }

    fun changeVolume(factor: Float): Sound {
        return if (factor == 1f) {
            this
        } else {
            val multipliedData = samples.map { it * factor }
                .map { it.coerceIn(-1f, 1f) }
                .toFloatArray()
            val multipliedHighestPeak = (highestPeak * factor).coerceAtMost(1f)
            Sound(multipliedData, multipliedHighestPeak, channels, sampleRate)
        }
    }

    fun convertToStereo(): Sound {
        val stereoSamples = when (channels) {
            1 -> samples.zip(samples) { a, b -> listOf(a, b) }.flatten().toFloatArray()
            2 -> samples
            else -> throw UnsupportedOperationException("Unsupported number of channels for stereo conversion: $channels")
        }
        return Sound(stereoSamples, highestPeak, 2, sampleRate)
    }

    fun applyEffects(vararg effects: Effect): Sound =
        effects.fold(this) { modifiedSound, effect -> effect.applyOn(modifiedSound) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (this::class.qualifiedName != other::class.qualifiedName) return false

        other as Sound

        if (!samples.contentEquals(other.samples)) return false
        if (highestPeak != other.highestPeak) return false
        if (channels != other.channels) return false
        if (sampleRate != other.sampleRate) return false
        if (frames != other.frames) return false

        return true
    }

    override fun hashCode(): Int {
        var result = samples.contentHashCode()
        result = 31 * result + highestPeak.hashCode()
        result = 31 * result + channels.hashCode()
        result = 31 * result + sampleRate.hashCode()
        result = 31 * result + frames.hashCode()
        return result
    }
}

expect fun Sound.resample(targetSampleRate: Int): Sound
