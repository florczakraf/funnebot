package filesystem

import okio.FileSystem
import okio.Path
import okio.Path.Companion.toPath

fun fileExists(path: String) = fileExists(path.toPath())
fun fileExists(path: Path) = FileSystem.SYSTEM.exists(path)

fun readFile(path: String): ByteArray = readFile(path.toPath())
fun readFile(path: Path): ByteArray = FileSystem.SYSTEM.read(path) { readByteArray() }

fun writeFile(path: String, content: String) {
    writeFile(path.toPath(), content)
}

fun writeFile(path: Path, content: String) {
    FileSystem.SYSTEM.write(path, mustCreate = false) { writeUtf8(content) }
}

fun createDirectory(path: String) {
    createDirectory(path.toPath())
}

fun createDirectory(path: Path) {
    FileSystem.SYSTEM.createDirectory(path, mustCreate = false)
}

fun listFilesInDirectory(path: String): List<Path> = listFilesInDirectory(path.toPath())
fun listFilesInDirectory(path: Path): List<Path> = FileSystem.SYSTEM.list(path)

fun Path.extension() = name.substringAfterLast(".", "")

fun Path.fileNameWithoutExtension() = name.substringBeforeLast(".")
