package irc

import com.soywiz.korio.net.ws.WebSocketClient

suspend fun WebSocketClient.pass(pass: String) {
    send("PASS $pass")
}

suspend fun WebSocketClient.nick(nick: String) {
    send("NICK $nick")
}

suspend fun WebSocketClient.join(channel: String) {
    send("JOIN #$channel")
}

suspend fun WebSocketClient.sendMessage(channel: String, message: String) {
    send("PRIVMSG #$channel :$message")
}
