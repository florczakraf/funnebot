package irc

import tts.LanguageCode

const val COMMAND_PREFIX = "!"

sealed class ChatCommand {
    abstract val source: Source
    abstract val ircData: IrcData
    abstract val tags: Tags
    abstract val message: String
}

data class SoundCommand(
    override val source: Source,
    override val ircData: IrcData,
    override val tags: Tags,
    override val message: String
) : ChatCommand() {

    private val command = message.drop(COMMAND_PREFIX.length).substringBefore(' ').lowercase()

    val soundName = command.substringBefore('-')
    val effects = command.substringAfter('-', "").split('-').filter { it.isNotBlank() }
}

data class TtsCommand(
    override val source: Source,
    override val ircData: IrcData,
    override val tags: Tags,
    override val message: String
) : ChatCommand() {

    private val command = message.drop(COMMAND_PREFIX.length).substringBefore(' ').lowercase()

    val languageCode = LanguageCode(command.substringBefore('-'))
    val text = message.substringAfter(' ', "")
    val effects = command.substringAfter('-', "").split('-').filter { it.isNotBlank() }
}

data class AutoReplyCommand(
    override val source: Source,
    override val ircData: IrcData,
    override val tags: Tags,
    override val message: String
) : ChatCommand() {

    val request = message.drop(COMMAND_PREFIX.length).substringBefore(' ').substringBefore('-').lowercase()
}

data class Source(val nick: String, val host: String) {

    companion object {
        fun parseFrom(ircSourceString: String): Source {
            // chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv
            val (nick, host) = ircSourceString.split('!')
            return Source(nick, host)
        }
    }
}

data class IrcData(val ircCommand: String, val channel: String) {

    companion object {
        fun parseFrom(ircDataString: String): IrcData {
            // PRIVMSG #chazoshtare
            val (ircCommand, channel) = ircDataString.split(' ')
            return IrcData(ircCommand, channel.substringAfter('#', ""))
        }
    }
}

data class Tags(
    val badges: Badges = Badges(),
    val moderator: Boolean = false,
    val subscriber: Boolean = false
) {

    companion object {
        fun parseFrom(rawIrcTagsString: String): Tags {
            val allTags = rawIrcTagsString.split(';')
                .map { it.split('=') }
                .associate { it[0] to it[1] }

            val allBadges = allTags["badges"]?.takeIf { it.isNotBlank() }
                ?.split(',')
                ?.map { it.split('/') }
                ?.associate { it[0] to it[1] } ?: emptyMap()

            return Tags(
                Badges(allBadges),
                "1" == allTags["mod"],
                "1" == allTags["subscriber"]
            )
        }
    }
}

data class Badges(val broadcaster: Boolean = false, val vip: Boolean = false) {

    constructor(badges: Map<String, String>) : this(
        "1" == badges["broadcaster"],
        "1" == badges["vip"]
    )
}
