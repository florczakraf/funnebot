package irc

import tts.LanguageCode

class ChatMessageParser(
    private val availableAutoReplyCommands: Set<String>,
    private val availableSounds: Set<String>,
    private val availableTtsLanguageCodes: Set<LanguageCode>
) {

    // https://dev.twitch.tv/docs/irc/example-parser/
    fun parseFromRawIrcString(rawIrcString: String): List<ChatCommand> {
        /*
        Example full strings:
        @badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type= :chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv PRIVMSG #chazoshtare :some test message
        @badge-info=;badges=moderator/1,partner/1;color=#5B99FF;display-name=StreamElements;emotes=;first-msg=0;flags=;id=d516931b-200a-42ae-b439-51c72c30d883;mod=1;room-id=87486142;subscriber=0;tmi-sent-ts=1636936504551;turbo=0;user-id=100135110;user-type=mod :streamelements!streamelements@streamelements.tmi.twitch.tv PRIVMSG #chazoshtare :@Chazoshtare, missing youtube ID, url or search
        */
        val ircString = rawIrcString.trim()
        var index = 1

        // @badge-info=subscriber/43;badges=broadcaster/1,subscriber/3012;client-nonce=29185e4ab41f8679eeffa1a97d560ff9;color=#FF69B4;display-name=Chazoshtare;emotes=;first-msg=0;flags=;id=76a9354a-1dd9-4fd8-acf2-8416ffb2fac0;mod=0;room-id=87486142;subscriber=1;tmi-sent-ts=1636936451875;turbo=0;user-id=87486142;user-type=
        val tags = if (ircString.startsWith('@')) {
            val tagsEndIndex = ircString.indexOf(' ')
            val rawTagsString = ircString.substring(index, tagsEndIndex)
            index = tagsEndIndex + 1
            Tags.parseFrom(rawTagsString)
        } else Tags()

        // chazoshtare!chazoshtare@chazoshtare.tmi.twitch.tv
        val source = if (ircString[index] == ':') {
            index++
            val sourceEndIndex = ircString.indexOf(' ', index)
            val rawSourceString = ircString.substring(index, sourceEndIndex)
            index = sourceEndIndex + 1
            Source.parseFrom(rawSourceString)
        } else Source("", "")

        // PRIVMSG #chazoshtare
        val ircDataEndIndex = ircString.indexOf(':', index)
            .let { if (it == -1) ircString.lastIndex else it }
        val rawIrcDataString = ircString.substring(index, ircDataEndIndex).trimEnd()
        val ircData = IrcData.parseFrom(rawIrcDataString)

        // some test message
        val content = if (ircDataEndIndex != ircString.lastIndex) {
            index = ircDataEndIndex + 1
            ircString.substring(index)
        } else ""

        val commands = content.split('+').mapNotNull {
            if (it.startsWith(COMMAND_PREFIX)) {
                val commandString = it.drop(COMMAND_PREFIX.length).substringBefore(' ').substringBefore('-')
                when {
                    commandString in availableAutoReplyCommands -> AutoReplyCommand(source, ircData, tags, it)
                    commandString in availableSounds -> SoundCommand(source, ircData, tags, it)
                    LanguageCode(commandString) in availableTtsLanguageCodes -> TtsCommand(source, ircData, tags, it)
                    else -> null
                }
            } else null
        }

        return commands
    }
}
