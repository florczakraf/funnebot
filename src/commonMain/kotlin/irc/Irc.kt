package irc

import audio.*
import com.soywiz.korio.net.ws.WebSocketClient
import com.soywiz.korio.net.ws.messageChannelString
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import tts.TTSConverter
import kotlin.system.exitProcess

private const val MAX_MESSAGE_LENGTH = 500
private const val MAX_TTS_LENGTH = 200
private const val TWITCH_WS_URL = "ws://irc-ws.chat.twitch.tv:80"

class Irc(
    private val audioPlayer: AudioPlayer,
    private val commands: Map<String, String>,
    private val sounds: Map<String, Sound>,
    ttsConverters: Set<TTSConverter>,
) {

    private val ttsCodesToConverters = ttsConverters.flatMap {
        it.supportedCodes.map { code -> code to it }
    }.toMap()

    private val chatMessageParser: ChatMessageParser = ChatMessageParser(
        commands.keys + setOf("sounds", "voices", "effects"),
        sounds.keys,
        ttsCodesToConverters.keys
    )

    suspend fun runIrcLoop(nick: String, oauth: String) = coroutineScope {
        val client = WebSocketClient(TWITCH_WS_URL) {
            onOpen { println("Websocket connection opened!") }
            onError {
                println("Websocket connection error - ${it.message}")
                exitProcess(21)
            }
            onClose {
                println("Websocket connection closed!")
                exitProcess(22)
            }
        }

        val messages = client.messageChannelString()
        launch {
            for (rawIrcMessage in messages) {
                rawIrcMessage as String

                when {
                    rawIrcMessage.startsWith("PING") -> client.send(rawIrcMessage.replace("PING", "PONG"))
                    rawIrcMessage.startsWith(":tmi.twitch.tv") -> println(rawIrcMessage)
                    else -> client.handleChatMessage(rawIrcMessage)
                }

            }
        }

        client.pass(oauth)
        client.nick(nick)
        client.join(nick)
        client.send("CAP REQ :twitch.tv/tags")

        while (true) {
            awaitCancellation()
//            delay(100)
        }
    }

    private suspend fun WebSocketClient.handleChatMessage(rawIrcMessage: String) {
        val chatCommands = chatMessageParser.parseFromRawIrcString(rawIrcMessage)

        if (chatCommands.isNotEmpty()) {
            println("[${chatCommands[0].source.nick}] ${chatCommands.joinToString("+", transform = ChatCommand::message)}")
            if (chatCommands.all { it is SoundCommand }) {
                handleMultiSoundCommand(chatCommands.filterIsInstance<SoundCommand>())
            } else {
                when (val command = chatCommands[0]) {
                    is SoundCommand -> handleSoundCommand(command)
                    is TtsCommand -> handleTtsCommand(command)
                    is AutoReplyCommand -> handleAutoReplyCommand(command)
                }
            }
        }
    }

    private suspend fun handleTtsCommand(ttsCommand: TtsCommand) = with(ttsCommand) {
        if (text.length in 1..MAX_TTS_LENGTH) {
            val audioEffects = convertToActualEffects(effects)
            val ttsSound = ttsCodesToConverters[languageCode]?.convertToSound(text, languageCode)
            if (ttsSound != null) {
                audioPlayer.play(ttsSound.applyEffects(*audioEffects), true)
            }
        }
    }

    private fun handleMultiSoundCommand(soundCommands: List<SoundCommand>) {
        val joinedSounds = soundCommands.mapNotNull {
            val audioEffects = convertToActualEffects(it.effects)
            sounds[it.soundName]?.applyEffects(*audioEffects)
        }.reduce { acc, sound -> acc + sound }
        audioPlayer.play(joinedSounds)
    }

    private fun handleSoundCommand(soundCommand: SoundCommand) = with(soundCommand) {
        val audioEffects = convertToActualEffects(effects)
        sounds[soundName]?.let {
            audioPlayer.play(it.applyEffects(*audioEffects))
        }
    }

    private suspend fun WebSocketClient.handleAutoReplyCommand(autoReplyCommand: AutoReplyCommand) = with(autoReplyCommand) {
        val response = commands[request]
        if (response != null) {
            sendMessage(ircData.channel, response)
        } else {
            when (request) {
                "sounds" -> handleSoundListCommand(autoReplyCommand)
                "voices" -> handleVoicesCommand(autoReplyCommand)
                "effects" -> handleEffectListCommand(autoReplyCommand)
            }
        }
    }

    private fun convertToActualEffects(effectsStrings: List<String>): Array<Effect> {
        val finalEffects = mutableListOf<Effect>()
        effectsStrings.map { effectFromName(it) }
            .filter { it !is None }
            .forEach { if (it is Reverse || it is Cut || !finalEffects.contains(it)) finalEffects.add(it) }
        return finalEffects.toTypedArray()
    }

    private suspend fun WebSocketClient.handleVoicesCommand(autoReplyCommand: AutoReplyCommand) {
        // TODO: don't exceed max character limit like in sounds list
        sendMessage(
            autoReplyCommand.ircData.channel,
            "TTS Voices: ${ttsCodesToConverters.keys.joinToString(", ") { it.value }}"
        )
    }

    private suspend fun WebSocketClient.handleEffectListCommand(autoReplyCommand: AutoReplyCommand) {
        val effectList = availableEffects.joinToString(", ") {
            "![sound]-${it.joinToString("/")}"
        }
        sendMessage(autoReplyCommand.ircData.channel, "$effectList [You can chain multiple effects!!1!!1!1]")
    }

    private suspend fun WebSocketClient.handleSoundListCommand(autoReplyCommand: AutoReplyCommand) {
        generateSoundsMessages(sounds).forEach {
            sendMessage(autoReplyCommand.ircData.channel, it)
            delay(1000)
        }
    }

    private fun generateSoundsMessages(sounds: Map<String, Sound>): List<String> {
        val separator = ", !"
        val messages = mutableListOf<String>()
        val soundsLeft = sounds.keys.toMutableList()
        while (soundsLeft.isNotEmpty()) {
            val message = StringBuilder("Sounds: !${soundsLeft.removeFirst()}")
            while (soundsLeft.isNotEmpty() && message.length + separator.length + soundsLeft[0].length < MAX_MESSAGE_LENGTH) {
                message.append(separator)
                message.append(soundsLeft.removeFirst())
            }
            messages.add(message.toString())
        }

        return messages
    }
}
