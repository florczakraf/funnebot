package irc

import filesystem.fileExists
import filesystem.listFilesInDirectory
import filesystem.readFile
import kotlinx.cinterop.toKString
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

@Serializable
private data class Command(
    val commands: Set<String>,
    val response: String
)

fun loadCommands(rootPath: String): Map<String, String> =
    if (fileExists(rootPath)) {
        listFilesInDirectory(rootPath)
            .map { readFile(it).toKString() }
            .map { Json.decodeFromString<Command>(it) }
            .flatMap { it.commands.map { command -> command to it.response } }
            .toMap()
    } else {
        println("Unable to load commands - directory \"$rootPath\" does not exist!")
        emptyMap()
    }
