fun String.containsAny(strings: Collection<String>) = strings.any { this.contains(it, true) }
fun String.containsAny(strings: Array<String>) = strings.any { this.contains(it, true) }
