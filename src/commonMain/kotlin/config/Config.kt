package config

import kotlinx.serialization.Serializable

@Serializable
data class Config(
    val nick: String,
    val oauth: String,
    val commandsPath: String?,
    val soundsPath: String?
)
