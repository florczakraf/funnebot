package audio

import kotlinx.cinterop.*
import samplerate.SRC_DATA
import samplerate.src_simple
import kotlin.math.ceil

actual fun Sound.resample(targetSampleRate: Int): Sound {
    if (sampleRate == targetSampleRate) return this

    val resamplingRatio = targetSampleRate.toDouble() / sampleRate.toDouble()
    val resampledSamplesSize = ceil(resamplingRatio * samples.size).toInt()
    val resampledSamples = FloatArray(resampledSamplesSize)

    memScoped {
        val srcData = alloc<SRC_DATA>().apply {
            data_in = samples.refTo(0).getPointer(this@memScoped)
            data_out = resampledSamples.refTo(0).getPointer(this@memScoped)
            input_frames = frames.convert()
            output_frames = resampledSamplesSize / channels
            src_ratio = resamplingRatio
        }

        src_simple(srcData.ptr, 0, channels)
    }

    return Sound(resampledSamples, channels, targetSampleRate)
}
