package audio.decoders

import audio.Sound
import kotlinx.cinterop.*
import platform.posix.SEEK_CUR
import platform.posix.SEEK_END
import platform.posix.SEEK_SET
import platform.posix.memcpy
import sndfile.*

actual object WavAudioDecoder : AudioDecoder {

    actual override fun decode(bytes: ByteArray): Sound {
        memScoped {
            val data = Data(bytes, 0)
            val dataStableRef = StableRef.create(data)

            val virtualIo = alloc<SF_VIRTUAL_IO>().apply {
                get_filelen = staticCFunction(::virtualGetFileLength)
                seek = staticCFunction(::virtualSeek)
                read = staticCFunction(::virtualRead)
                tell = staticCFunction(::virtualTell)
//                write =
            }

            val sfInfo = alloc<SF_INFO>()
            val file = sf_open_virtual(virtualIo.ptr, SFM_READ.toInt(), sfInfo.ptr, dataStableRef.asCPointer())
            if (SF_ERR_NO_ERROR.toInt() != sf_error(file)) {
                val errorMessage = sf_strerror(file)!!.toKString()
                sf_close(file)
                throw AudioDecoderException("Error while decoding WAV: $errorMessage")
            }

            val samplesCount = sfInfo.frames * sfInfo.channels
            val samples = FloatArray(samplesCount.convert())
            sf_readf_float(file, samples.refTo(0), sfInfo.frames)
            sf_close(file)

            return Sound(samples, sfInfo.channels, sfInfo.samplerate)
        }
    }
}

private fun virtualGetFileLength(userData: COpaquePointer?): Long {
    val data = userData!!.asStableRef<Data>().get()
    return data.size.convert()
}

private fun virtualSeek(offset: Long, whence: Int, userData: COpaquePointer?): Long {
    val data = userData!!.asStableRef<Data>().get()
    when (whence) {
        SEEK_SET -> data.position = offset.convert()
        SEEK_CUR -> data.position = (data.position + offset).convert()
        SEEK_END -> data.position = (data.lastIndex + offset).convert()
    }
    return data.position.convert()
}

private fun virtualRead(destination: COpaquePointer?, count: Long, userData: COpaquePointer?): Long {
    val data = userData!!.asStableRef<Data>().get()
    val bytesToRead = if (data.position + count > data.lastIndex) data.remaining else count.toInt()
    memcpy(destination, data.bytes.refTo(data.position), bytesToRead.convert())
    data.position += bytesToRead
    return bytesToRead.convert()
}

private fun virtualTell(userData: COpaquePointer?): Long {
    val data = userData!!.asStableRef<Data>().get()
    return data.position.convert()
}

private class Data(
    val bytes: ByteArray,
    var position: Int
) {

    val size get() = bytes.size
    val lastIndex get() = size - 1
    val remaining get() = size - position
}
