# Funnebot

Twitch chatbot for all extremely funny things, like playing sounds or TTS with custom effects and possibly much more.

## Features

Default commands:

- `!sounds` - list all sounds
- `!effects` - list all effects
- `!voices` - list all TTS voices

Effects available: earrape, reverse, fast, slow, low quality, cut, more in the future

**Playing sounds:**

- `![sound]` - to play a sound
- `![sound]-[effect]` - to play a sound with effect

**Playing TTS** - you need to send language code and a message:

- `![voice] message` - to play TTS
- `![voice]-[effect] message` - to play TTS with effect

For example `!us-reversed This is a super funny TTS message!!!!`  
You can also chain multiple effects, for example `!sound-er-f-lq`  
Currently Google and TikTok TTSs are supported.

**Custom commands:**

Custom commands can be defined with json files as follows:

```json
{
  "commands": [
    "mycommand",
    "alias1",
    "alias2"
  ],
  "response": "Response to custom command."
}
```

Each file should represent a separate command. Put them all together in a single folder and configure the path as
described in **"Running"** section. Custom commands can override previously mentioned default commands.

## Dependencies

This bot requires `curl`, `portaudio`, `libsamplerate` and `libsndfile`. Install them on whatever linux distro you use,
or install them with MSYS on Windows.

## Running

Make sure dependencies are installed. Create a config file right next to funnebot executable called `config.json`. It
needs to look like this:

```json
{
  "nick": "[your-nick]",
  "oauth": "[oauth:XXXXXXXXX]",
  "commandsPath": "[path-to-commands-folder]",
  "soundsPath": "[path-to-sounds-folder]"
}
```

You can generate `oauth` token here:  https://twitchapps.com/tmi/

## Building

Make sure dependencies are installed. Then use:

```shell
# For Linux
./gradlew buildLinuxX64
```

```shell
# For Windows
./gradlew buildMingwX64
```

Use the executable gererated in `/build/bin/[platform]/releaseExecutable`
