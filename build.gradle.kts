import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

plugins {
    kotlin("multiplatform") version "1.8.0"
    kotlin("plugin.serialization") version "1.8.0"
}

group = "com.sneknology"
version = "0.0.1"

repositories {
    mavenCentral()
}

tasks.register("buildLinuxX64") {
    val testTaskName = "linuxX64Test"
    val linkReleaseTaskName = "linkReleaseExecutableLinuxX64"
    val linkDebugTaskName = "linkDebugExecutableLinuxX64"
    dependsOn(testTaskName, linkReleaseTaskName, linkDebugTaskName)
    tasks.named(linkReleaseTaskName).get().mustRunAfter(testTaskName)
    tasks.named(linkDebugTaskName).get().mustRunAfter(testTaskName)
}

tasks.register("buildMingwX64") {
    val testTaskName = "mingwX64Test"
    val linkReleaseTaskName = "linkReleaseExecutableMingwX64"
    val linkDebugTaskName = "linkDebugExecutableMingwX64"
    dependsOn(testTaskName, linkReleaseTaskName, linkDebugTaskName)
    tasks.named(linkReleaseTaskName).get().mustRunAfter(testTaskName)
    tasks.named(linkDebugTaskName).get().mustRunAfter(testTaskName)
}

kotlin {
    linuxX64().apply {
        binaries.executable {
            entryPoint = "main"
        }
    }
    mingwX64().apply {
        binaries.executable {
            entryPoint = "main"
        }
    }

    targets.withType<KotlinNativeTarget> {
        compilations["main"].cinterops {
            val portaudio by creating
            val sndfile by creating
            val minimp3 by creating
            val samplerate by creating
        }

        // Disable cross-compilation workaround until: https://youtrack.jetbrains.com/issue/KT-30498
//        if (konanTarget != HostManager.host) {
//            compilations.all {
//                cinterops.all { tasks[interopProcessingTaskName].enabled = false }
//                compileKotlinTask.enabled = false
//            }
//            binaries.all { linkTask.enabled = false }
//        }
    }

    sourceSets {
        val korlibsVersion = "3.4.0"
        val ktorVersion = "2.2.2"

        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.4.1")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
                implementation("com.soywiz.korlibs.korio:korio:$korlibsVersion")
                implementation("io.ktor:ktor-client-core:$ktorVersion")
                implementation("io.ktor:ktor-client-curl:$ktorVersion")
                implementation("io.ktor:ktor-client-content-negotiation:$ktorVersion")
                implementation("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
                implementation("com.squareup.okio:okio:3.2.0")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }

        val desktopMain by creating {
            dependsOn(commonMain)
        }
        val desktopTest by creating {
            dependsOn(commonTest)
        }

        val linuxX64Main by getting {
            dependsOn(desktopMain)
        }
        val linuxX64Test by getting {
            dependsOn(desktopTest)
        }
        val mingwX64Main by getting {
            dependsOn(desktopMain)
        }
        val mingwX64Test by getting {
            dependsOn(desktopTest)
        }

        all {
            // Used for ExperimentalUnsignedTypes
            languageSettings.optIn("kotlin.RequiresOptIn")
        }
    }
}
